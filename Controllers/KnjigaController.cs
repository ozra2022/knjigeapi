#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KnjigeApi.Models;

namespace KnjigeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KnjigaController : ControllerBase
    {
        private readonly KnjigaContext _context;

        public KnjigaController(KnjigaContext context)
        {
            _context = context;
        }

        // GET: api/Knjiga
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Knjiga>>> GetKnjige()
        {
            return await _context.Knjige.ToListAsync();
        }

        // GET: api/Knjiga/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Knjiga>> GetKnjiga(Guid id)
        {
            var knjiga = await _context.Knjige.FindAsync(id);

            if (knjiga == null)
            {
                return NotFound();
            }

            return knjiga;
        }

        // PUT: api/Knjiga/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKnjiga(Guid id, Knjiga knjiga)
        {
            if (id != knjiga.Id)
            {
                return BadRequest();
            }

            _context.Entry(knjiga).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KnjigaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Knjiga
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Knjiga>> PostKnjiga(Knjiga knjiga)
        {
            _context.Knjige.Add(knjiga);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKnjiga", new { id = knjiga.Id }, knjiga);
        }

        // DELETE: api/Knjiga/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteKnjiga(Guid id)
        {
            var knjiga = await _context.Knjige.FindAsync(id);
            if (knjiga == null)
            {
                return NotFound();
            }

            _context.Knjige.Remove(knjiga);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool KnjigaExists(Guid id)
        {
            return _context.Knjige.Any(e => e.Id == id);
        }
    }
}
