namespace KnjigeApi.Models
{
    public class Knjiga
    {
        public Guid Id { get; set; }
        public string  Avtor { get; set; }
        public string Naslov { get; set; }
        public string Isbn { get; set; }
        public int LetoIzida { get; set; }
        
    }
}