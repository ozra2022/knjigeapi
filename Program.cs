using KnjigeApi.Models;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var server = builder.Configuration["DBServer"]??"ms-sql-server";
var port = builder.Configuration["DBPort"]??"1433";
var user = builder.Configuration["DBUser"]??"SA";
var password = builder.Configuration["DBPassword"]??"S3CreT_Pa550rd!2o22";
var dbname = builder.Configuration["DBName"]??"Knjige";
var connString = $"Server={server},{port};Initial Catalog={dbname};User Id={user};Password={password};";

builder.Services.AddDbContext<KnjigaContext>(options => options.UseSqlServer(connString));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

KnjigeSeeder.SeedData(app);

app.UseAuthorization();

app.MapControllers();

app.Run();
